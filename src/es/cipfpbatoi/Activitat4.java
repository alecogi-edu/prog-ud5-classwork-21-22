package es.cipfpbatoi;

import java.util.Scanner;

/**
 * Activitat 4.- Escriu un mètode que, donat un String, torni un altre objecte String
 * en què es canvien totes les vocals minúscules de l'original per la lletra 'a'.
 * Escriu un programa que permeta comprovar-ne el funcionament.
 */
public class Activitat4 {

    private static Scanner teclat;

    public static void main(String[] args) {

        teclat = new Scanner(System.in);
        String num1 = demanarDoubleComText();
        String num2 = demanarDoubleComText();

        System.out.println("Suma : " + obtindreSuma(num1, num2));
        System.out.println("Multiplicacio : " + obtindreMultiplicacio(num1, num2));

    }

    public static double obtindreSuma(String num1, String num2) {

        double num1Double = Double.valueOf(num1);
        double num2Double = Double.valueOf(num2);
        return num1Double + num2Double;

    }

    public static double obtindreMultiplicacio(String num1, String num2) {
        double num1Double = Double.valueOf(num1);
        double num2Double = Double.valueOf(num2);
        return num1Double * num2Double;
    }

    public static String demanarDoubleComText() {

        System.out.println("Introdueix un numero");
        return teclat.next();

    }

}
