package es.cipfpbatoi;

/**
 * Activitat 3.- Escriu un mètode que, atesa una cadena de caràcters,
 * torne un altre objecte de tipus String que contingui la meitat inicial
 * de la cadena. Escriu un programa que prove el mètode amb les cadenes
 *  “Hola que tal” i “Adéu”.
 */
public class Activitat3 {

    public static void main(String[] args) {

        System.out.println(donamMitatInicial("Hola que tal"));
        System.out.println(donamMitatInicial("Adéu"));

    }

    public static String donamMitatInicial(String cadena) {

        return cadena.substring(0, (cadena.length()) / 2);

    }
}
