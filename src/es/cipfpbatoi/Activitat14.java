package es.cipfpbatoi;

import java.util.Scanner;

public class Activitat14 {

    public static void main(String[] args) {

        int[] listado = new int[10];
        Scanner teclat = new Scanner(System.in);

        for (int i = 0; i < listado.length; i++) {
            System.out.println("Introuce el elemento " + i);
            listado[i] = teclat.nextInt();
        }

        for (int item: listado) {
            System.out.println(item);
        }
    }
}
