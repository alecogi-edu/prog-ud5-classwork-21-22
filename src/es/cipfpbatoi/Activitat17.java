package es.cipfpbatoi;

import java.util.Random;

public class Activitat17 {

    public static void main(String[] args) {

        int[] myArray = crearArray();
        visualitzarArray(myArray);
        System.out.println("El primer 0 está en la posició" + cercarZero(myArray));
        intercanvia(myArray);
        visualitzarArray(myArray);

    }

    /**
     * Declara e inicialitza un array de 10 elements amb números aleatoris del 0 al 50.
     *
     * @return
     */
    public static int[] crearArray() {
        int[] llistat = new int[10];
        Random generadorAleatoris = new Random();
        for (int i = 0; i < llistat.length; i++) {
            llistat[i] = generadorAleatoris.nextInt(51);
        }
        return llistat;
    }

    /**
     * visualitza tot el contingut de l'array.
     * @param vector
     */
    public static void visualitzarArray(int[] vector) {
        for (int item: vector) {
            System.out.print(item + ",");
        }
        System.out.println();
    }

    /**
     * cercarà la primera posició del primer element amb contingut 0 i tornarà el seu índex. Si no hi ha cap element amb contingut 0, tornarà un -1.
     * @param vector
     * @return
     */
    public static int cercarZero(int[] vector) {
        for (int i = 0; i < vector.length; i++) {
            if (vector[i] == 0) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Intercanvia els valors de la primera i última posició sempre que la seva longitud (número de elements) siga major o igual que 2.
     * @param vector
     */
    public static void intercanvia(int[] vector) {
        if (vector.length >= 2) {
            int primerElement = vector[0];
            int ultimElement = vector[vector.length-1];
            vector[0] = ultimElement;
            vector[vector.length-1] = primerElement;
        }
    }

}
