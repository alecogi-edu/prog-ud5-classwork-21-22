package es.cipfpbatoi;

import java.util.Arrays;
import java.util.Scanner;

public class Activitat22 {

    public static void main(String[] args) {

        int[] llistatNoOrdenat = Activitat20.crearArrayElements(10);
        int elementABuscar = demanarEnter();
        int indexElement = cercarEnVectorNoOrdenat(llistatNoOrdenat, elementABuscar);
        System.out.println(Arrays.toString(llistatNoOrdenat));
        System.out.printf("El element %d està en el index %d %n",elementABuscar, indexElement);

        int[] llistatOrdenat = Activitat20.crearArrayElements(10);
        Activitat20.ordenarPerIntercanvi(llistatOrdenat);
        System.out.println(Arrays.toString(llistatOrdenat));
        indexElement = cercarEnVectorOrdenat(llistatOrdenat, elementABuscar);
        System.out.printf("El element %d està en el index %d %n",elementABuscar, indexElement);

    }

    public static int cercarEnVectorNoOrdenat(int[] numeros, int numBuscar) {
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == numBuscar) {
                return i;
            }
        }
        return -1;
    }

    public static int cercarEnVectorOrdenat(int[] numeros, int numBuscar) {
        for (int i = 0; i < numeros.length; i++) {
            if (numeros[i] == numBuscar) {
                return i;
            } else if (numeros[i] > numBuscar) {
                break;
            }
        }
        return -1;
    }

    public static int demanarEnter() {
        Scanner teclat = new Scanner(System.in);
        System.out.println("Introdueix un enter");
        return teclat.nextInt();
    }
}
