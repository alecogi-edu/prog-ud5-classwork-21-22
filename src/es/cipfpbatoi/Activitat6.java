package es.cipfpbatoi;

import java.util.Scanner;

public class Activitat6 {

    public static void main(String[] args) {
        String nomComplet = obtindreNomComplet();
        mostrarCasA(nomComplet);
        mostrarPrimersCaracters(nomComplet);
        mostrar1ErCaracterMajuscules(nomComplet);
        mostrarAmbAsteriscs(nomComplet);
        mostraInvertit(nomComplet);
    }

    private static void mostraInvertit(String nomComplet) {
        for (int i = nomComplet.length() - 1; i >= 0 ; i--) {
            System.out.print(nomComplet.charAt(i));
        }
    }

    public static void mostrarCasA(String nomComplet) {
        System.out.println("Minúscules: " + nomComplet.toLowerCase());
        System.out.println("Majúscules: " + nomComplet.toUpperCase());
        System.out.println("Longitud: " + nomComplet.length());
    }

    public static void mostrarPrimersCaracters(String nomComplet) {
        if (nomComplet.length() >= 2) {
            System.out.println("2 primers caracters: " + nomComplet.substring(0,2));
        }
    }

    public static void mostrar1ErCaracterMajuscules(String nomComplet){
        String caracter = nomComplet.substring(0, 1);
        String nomCompletSubstituit = nomComplet.replaceAll(caracter.toLowerCase(), caracter.toUpperCase());
        System.out.println("1er Caracter Majúscules: " + nomCompletSubstituit);
    }

    public static void mostrarAmbAsteriscs(String nomComplet) {
        System.out.println("Amb Asteriscs: ***".concat(nomComplet).concat("***"));
    }

    public static String obtindreNomComplet() {
        Scanner teclat = new Scanner(System.in);
        System.out.println("Introdueix el nom: ");
        String nom = teclat.next();
        System.out.println("Introdueix el cognom1: ");
        String cognom1 = teclat.next();
        System.out.println("Introdueix el cognom2: ");
        String cognom2 = teclat.next();
        return nom + " " + cognom1 + " " + cognom2;
    }

}
