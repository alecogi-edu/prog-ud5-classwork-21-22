package es.cipfpbatoi;


import java.util.Arrays;

/**
 * Muestre la tabla completa.
 * Muestre la información de los alumnos matriculados en DAW y ASIX.
 * Muestre la media de edad de los alumnos matriculados en el centro.
 * Promocione a los alumnos de primer curso al segundo curso.
 * Cree una nueva tabla de 3x2 que almacena el total de alumnos matriculados en DAW, DAM y ASIX
 */
public class Activitat26 {

    public static final int COLUMN_AGE = 2;
    public static final int COLUMN_CYCLE = 3;
    public static final int COLUMN_COURSE = 4;

    public static void main(String[] args) {

        String[][] students = {
                {"Juan", "Perez Aura", "24", "ASIX", "1"},
                {"Maria", "Sanchez García", "18", "DAW","1"},
                {"Pepa", "Egea Juan", "21","DAM", "1"},
                {"Ana Maria", "Hernandez Julian", "20", "DAW", "2"},
                {"Francisco", "Juan Juan", "28", "DAW", "1"},
        };

        showStudentsInTable(students);
        showAlumnos(students, "DAW");
        showAlumnos(students, "ASIX");
        showAverageAge(students);

        System.out.println("Promocionando alumnos...");
        promoteSchoolars(students);
        showStudentsInTable(students);

        System.out.println("Reagrupando datos...");
        String[][] summaryData = getSchoolarsCount(students);

        showScholarsPerYear(summaryData);

    }

    public static void showAlumnos(String[][] students, String cicle){

        for (int i = 0; i < students.length ; i++) {
            if (students[i][COLUMN_CYCLE].equals(cicle)){
                Arrays.toString(students[i]);
            }
        }

    }

    public static void promoteSchoolars(String[][] students){

        for (int i = 0; i < students.length ; i++) {
            if (students[i][COLUMN_COURSE].equals("1")){
                students[i][COLUMN_COURSE] = "2";
            }
        }

    }

    public static void showAverageAge(String[][] students){

        int totalEdad = 0;
        for (int i = 0; i < students.length ; i++) {
            totalEdad += Integer.parseInt(students[i][COLUMN_AGE]);
        }
        System.out.println("Total edad media alumnos: " + (totalEdad / students.length));

    }

    public static void showStudentsInTable(String[][] students) {

        System.out.println("+------------------+------------------+------------------+------------------+-----------------+");
        for (String[] fila: students) {
            System.out.printf("|");
            for (String elemento: fila) {
                System.out.printf(" %-16s |", elemento);
            }
            System.out.println("\n+------------------+------------------+------------------+------------------+-----------------+");
        }

    }

    public static String[][] getSchoolarsCount(String[][] students){

        int totalStudentsDAW = 0;
        int totalStudentsDAM = 0;
        int totalStudentsASIX = 0;

        for (int i = 0; i < students.length; i++) {
            if (students[i][COLUMN_CYCLE].equals("DAW")) {
                totalStudentsDAW++;
            } else if(students[i][COLUMN_CYCLE].equals("DAM")) {
                totalStudentsDAM++;
            } else {
                totalStudentsASIX++;
            }
        }

        return new String[][] {
                {"DAW", String.valueOf(totalStudentsDAW)},
                {"DAM", String.valueOf(totalStudentsDAM)},
                {"ASIX", String.valueOf(totalStudentsASIX)}
        };

    }

    public static void showScholarsPerYear(String[][] summaryData) {

        for (String[] file: summaryData) {
            System.out.printf("%s --> %s %n", file[0], file[1]);
        }

    }
}
