package es.cipfpbatoi;

import java.util.Arrays;
import java.util.Random;

public class Activitat20 {

    static Random generadorAleatoris = new Random();

    public static void main(String[] args) {

        int[] llistat1 = crearArrayElements(10);
        int[] llistat2 = crearArrayElements(10);
        System.out.println(Arrays.toString(llistat1));
        System.out.println(Arrays.toString(llistat2));
        ordenarPerIntercanvi(llistat1);
        ordenarPerIntercanvi(llistat2);
        System.out.println(Arrays.toString(llistat1));
        System.out.println(Arrays.toString(llistat2));

    }

    public static int[] crearArrayElements(int numElements) {
        int[] llistat = new int[numElements];
        for (int i = 0; i < llistat.length; i++) {
            llistat[i] = generadorAleatoris.nextInt(100);
        }
        return llistat;
    }

    public static void ordenarPerIntercanvi(int[] llistat) {
        for (int i = 0; i < llistat.length - 1 ; i++) {
            for (int j = i + 1; j < llistat.length; j++) {
                if (llistat[j] < llistat[i]) {
                    int menor = llistat[j];
                    llistat[j] = llistat[i];
                    llistat[i] = menor;
                }
            }
        }

    }

}
