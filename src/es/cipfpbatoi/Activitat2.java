package es.cipfpbatoi;

/**
 * Activitat 2.- Escriu un mètode que donada una cadena passada com a argument mostri tots els caràcters de la cadena separats pel caràcter '-' ;
 * Exemple: per a la cadena "Hola Món" ha de Mostrar "H-o-l-a-M-ó-n"
 */
public class Activitat2 {

    public static void main(String[] args) {

        mostrarAmbBarres("Hola Món");
        mostrarAmbBarres("Hola Juan");
        mostrarAmbBarres("Qué tal");

    }

    public static void mostrarAmbBarres(String missatge) {

        for (int i = 0; i < missatge.length(); i++) {
            char actual = missatge.charAt(i);
            if (actual != ' ') {
                System.out.print(actual);
                if (i < missatge.length() - 1) {
                    System.out.print('-');
                }
            }
        }
        System.out.println();
    }

}
