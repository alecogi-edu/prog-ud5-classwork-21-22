package es.cipfpbatoi;

import java.util.Random;
import java.util.Scanner;

/**
 * Activitat 5.15.- Escriu un programa que genere un array de 20 elements
 * amb valors aleatoris de tipus enter. A continuació
 * els ha de mostrar de 4 en 4 utilitzant l'estructura foreach
 */
public class Activitat15 {

    public static void main(String[] args) {

        Random generadorAleatorios = new Random();

        int[] listado = new int[20];
        for (int i = 0; i < listado.length; i++) {
            listado[i] = generadorAleatorios.nextInt(10);
        }

        int contador = 1;
        for (int numero: listado) {
            System.out.print(numero);
            if (contador % 4 == 0) {
                System.out.print(" ");
            }
            contador++;
        }

    }
}
