package es.cipfpbatoi;

import java.util.Scanner;

public class Activitat12 {

    public static void main(String[] args) {

        int[] numeros = new int[10];
        Scanner teclat = new Scanner(System.in);

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("Introuce el elemento " + i);
            numeros[i] = teclat.nextInt();
        }

        for (int i = 0; i < numeros.length; i++) {
            System.out.println("elemento " + i +" es " + numeros[i]);
        }

    }
}
