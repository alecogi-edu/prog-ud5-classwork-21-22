package es.cipfpbatoi;

import java.util.Scanner;

public class Activitat13 {

    public static void main(String[] args) {

        String[] listado = new String[12];
        Scanner teclat = new Scanner(System.in);

        for (int i = 0; i < listado.length; i++) {
            listado[i] = teclat.next();
        }

        for (int i = 0; i < listado.length; i+=2) {
            System.out.println(listado[i]);
        }

        for (int i = 0; i < listado.length; i++) {
            if (i % 2 == 0) {
                System.out.println(listado[i]);
            }
        }

    }
}
