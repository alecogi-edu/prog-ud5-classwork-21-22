package es.cipfpbatoi;

/**
 * Activitat 1.- Escriu un mètode que donada una cadena passada com a argument torneu un booleà indicant si es tracta d'una salutació o no.
 * Es consideren salutacions les paraules ("Hola", "Hello" i "Què tal")
 */
public class Activitat1 {

    public static void main(String[] args) {
	// write your code here
    }

    public static boolean esUnSaludo(String cadena) {

        return (cadena.equalsIgnoreCase("Hola")
                || cadena.equalsIgnoreCase("Hello")
                || cadena.equalsIgnoreCase("Què tal"));

    }
}
