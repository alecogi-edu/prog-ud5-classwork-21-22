package es.cipfpbatoi;

import java.util.Arrays;

public class Activitat21 {

    public static void main(String[] args) {

        String[] alumnes = {
                "Eric", "Juan Gregori", "Adrian", "Miquel", "Jose",
                "Adrìa", "Sergi", "Alvaro", "Iker", "Kessvan", "Ruben", "Alexis",
                "Victor", "Isaac", "Pau", "Mauro", "Jorge", "Cecilia", "Raquel"
        };

        System.out.println(Arrays.toString(alumnes));
        ordenarPerSeleccio(alumnes);
        System.out.println(Arrays.toString(alumnes));

    }

    public static void ordenarPerSeleccio(String[] llistat) {
        for (int i = 0; i < llistat.length - 1 ; i++) {
            int indexMenor = i;
            for (int j = indexMenor + 1; j < llistat.length; j++) {
                if (llistat[j].compareTo(llistat[indexMenor]) > 0) {
                    indexMenor = j;
                }
            }
            if (indexMenor != i) {
                String menor = llistat[indexMenor];
                llistat[indexMenor] = llistat[i];
                llistat[i] = menor;
            }
        }
    }
}
