package es.cipfpbatoi;

import java.util.Random;

public class Activitat25 {

    public static void main(String[] args) {
        int[][] matriu = crearArray();
        veureMatriu(matriu);
        visualitzarFila(matriu, 5);
        System.out.println("Intercanvia Elements");
        intercanvia(matriu, 4, 3, 5, 3);
        veureMatriu(matriu);
        visualitzarFila(matriu, 1);
        visualitzarFila(matriu, 4);
        veureMatriu(matriu);
        intercanviaColumnes(matriu, 1, 4);
        veureMatriu(matriu);
    }

    private static void intercanviaColumnes(int[][] matriu, int columna1, int columna2) {
        for (int i = 1; i <= matriu.length ; i++) {
            intercanvia(matriu, i, columna1, i, columna2);
        }
    }

    public static void visualitzarFila(int[][] matriu, int fila) {
        System.out.println("Mostrando fila " + fila);
        for (int i = 0; i < matriu[fila - 1].length ; i++) {
            System.out.print(matriu[fila - 1][i] + ",");
        }
        System.out.println();
    }

    public static int[][] crearArray() {
        Random random = new Random();
        int[][] matriu = new int[5][7];
        for (int i = 0; i < matriu.length; i++) {
            for (int j = 0; j < matriu[i].length; j++) {
                matriu[i][j] = random.nextInt(10);
            }
        }
        return matriu;
    }

    public static void veureMatriu(int[][] matriu){
        System.out.println(" --- Veure Matriu ----");
        for (int i = 0; i < matriu.length; i++) {
            for (int j = 0; j < matriu[i].length; j++) {
                System.out.print(matriu[i][j] + ",");
            }
            System.out.println();
        }
        System.out.println(" ---------------");
    }

    public static void intercanvia(int[][] matriu, int x1, int y1, int x2, int y2) {
        int aux = matriu[x1 - 1][y1 - 1];
        matriu[x1 - 1][y1 - 1] = matriu[x2 - 1][y2 - 1];
        matriu[x2 - 1][y2 - 1] = aux;
    }
}
