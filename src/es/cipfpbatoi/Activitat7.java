package es.cipfpbatoi;

public class Activitat7 {

    public static void main(String[] args) {

        System.out.printf("Es capicua %b \n", isCapICua("capicua"));
        System.out.printf("Es capicua %b \n", isCapICua("radar"));
        System.out.printf("Es capicua %b \n", isCapICua("somos"));
        System.out.printf("Es capicua %b \n", isCapICua("conocer"));
        System.out.printf("Es capicua %b \n", isCapICua("reconocer"));

    }

    public static boolean isCapICua(String cadena){
        int rightIndex = cadena.length() - 1;
        int leftIndex = 0;
        while (leftIndex < rightIndex) {
            if (cadena.charAt(leftIndex) != cadena.charAt(rightIndex)) {
                return false;
            }
            leftIndex++;
            rightIndex--;
        }
        return true;
    }
}
